//selection des bouttons
let boutton1 = document.getElementById("rouge");
let boutton2 = document.getElementById("orange");
let boutton3 = document.getElementById("jaune");
let boutton4 = document.getElementById("vert");
let boutton5 = document.getElementById("bleu");
let boutton6 = document.getElementById("turquoise");
let boutton7 = document.getElementById("rose");
let allButton = [document.querySelectorAll(".butt")];

//interaction sphere
let rond = [document.querySelectorAll(".cercle")];
let couleur = [document.querySelectorAll(".rand")];

let size = [
    "200px",
    "250px",
    "300px",
    "350px",
    "400px",
    "450px",
    "500px",
    "550px",
    "600px",
    "650px",
    "700px",
];

let move = [
    "425ms",
    "850ms",
    "1275ms",
    "1700ms",
    "2125ms",
    "2550ms",
    "2975ms",
    "3400",
];

let soundsArr = [
    "assets/sound/Ogg/170_Cm_808Bass_01_662.ogg",
    "assets/sound/Ogg/170_CymbalsBeat_02_662.ogg",
    "assets/sound/Ogg/170_DnBBreak_01_662.ogg",
    "assets/sound/Ogg/170_DnBBreak_02_662.ogg",
    "assets/sound/Ogg/170_FullBeat_01_662.ogg",
    "assets/sound/Ogg/170_FullBeat_03_662.ogg",
    "assets/sound/Ogg/170_Gm_DistortedBass_01_662.ogg",
    "assets/sound/Ogg/170_Gm_DistortedBass_03_662.ogg",
    "assets/sound/Ogg/170_Gm_MinimalBass_02_662.ogg",
    "assets/sound/Ogg/170_Gm_MoovingPad_01_662.ogg",
    "assets/sound/Ogg/170_Gm_StabSynth_01_662.ogg",
    "assets/sound/Ogg/170_Gm_StabSynth_02_662.ogg",
    "assets/sound/Ogg/170_SyncopatedBeat_02_662.ogg",
    "assets/sound/Ogg/170_SyncopatedBeat_04_662.ogg",
    "assets/sound/Ogg/170_SyncopatedFill_01_662.ogg",
];

//fin de jeu
let next = document.querySelector(".next");
let cercle = document.querySelector(".plan");
let gameOver = document.getElementById("gameover");
let progBar = document.getElementById("bar");
let clickbtn = false;
let timer;

//score
let plus = 00;

//creation de la class Sphere avec ses differents elements
class Sphere {
    constructor(color, width, speed, sound) {
        this.color = color;
        this.width = width;
        this.speed = speed;
        this.sound = sound;
    }
    //fonction qui change une couleur à cliquer aleatoirement
    chngColor() {
        let randColor = Math.floor(Math.random() * this.color.length);
        this.color = this.color[randColor];
    }
    //fonction qui change la taille de la sphere aléatoirement
    chngWidth() {
        let randWidth = Math.floor(Math.random() * this.width.length);
        this.width = this.width[randWidth];
        return this.width;
    }
    //fonction qui change la vitesse de la sphere aleatoirement
    chngSpeed() {
        let randSpeed = Math.floor(Math.random() * this.speed.length);
        this.speed = this.speed[randSpeed];
        return this.speed;
    }
    //fonction qui choisit un son aleatoirement
    chngSound() {
        let randSound = Math.floor(Math.random() * this.sound.length);
        this.sound = this.sound[randSound];
        return this.sound;
    }
    //fonction si mauvais click
    err() {
        clearTimeout(timer);
        next.style.display = "none";
        cercle.style.display = "none";
        gameOver.style.display = "block";
        document.querySelector(".config").style.display = "none";
        progBar.remove();
    }
    //fonction si bon click
    good() {
        clearTimeout(timer);
        let audio = new Audio(this.chngSound());
        audio.play();
        setTimeout(() => { audio.pause() }, 8500);
        this.chngColor();
        this.chngWidth();
        this.chngSpeed();
        bar.remove();
        document.body.appendChild(progBar);
        setTimeout(() => { game() }, 200);
    }
}

//instanciation d'un nouvel objet Sphere
let sphere = new Sphere(couleur, size, move, soundsArr);

//fonction qui lance le timeout et les actions suite au gameover
function timeLapse() {
    timer = setTimeout(() => {
        next.style.display = "none";
        cercle.style.display = "none";
        gameOver.style.display = "block";
        document.querySelector(".config").style.display = "none";
        bar.remove();
    }, 1700);
}

//fonction qui rend les bouttons actifs
function clkBut(but) {
    this.addEventListener("click", newClick)
}

// l'inverse
function rmvBut(but) {
    this.removeEventListener("click", newClick);
}

function newClick(e) {
    clickbtn = e.target.id;
    if (sphere.color.id === "red") {
        if ((clickbtn === "btnR") || (clickbtn === "rouge")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "rouge") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "ora") {
        if ((clickbtn === "btnO") || (clickbtn === "orange")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "orange") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "yel") {
        if ((clickbtn === "btnJ") || (clickbtn === "jaune")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "jaune") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "gre") {
        if ((clickbtn === "btnV") || (clickbtn === "vert")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "vert") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "blu") {
        if ((clickbtn === "btnB") || (clickbtn === "bleu")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "bleu") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "tur") {
        if ((clickbtn === "btnT") || (clickbtn === "turquoise")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "turquoise") {
            sphere.err();
        }
    }
    else if (sphere.color.id === "pin") {
        if ((clickbtn === "btnRo") || (clickbtn === "rose")) {
            plus++;
            rmvBut(e);
            sphere.color.style.visibility = "hidden";
            sphere = null;
            sphere = new Sphere(couleur, size, move, soundsArr);
            cercle.style.width = sphere.chngWidth();
            cercle.style.animationDuration = sphere.chngSpeed();
            sphere.good();
        }
        else if (clickbtn !== "rose") {
            sphere.err();
        }
    }
}

//fonction qui lance le jeu avec les etats aleatoire de la sphere
function game() {
    sphere.chngColor();
    sphere.color.style.visibility = "visible";
    document.getElementById("score").innerText = plus;
    timeLapse();
    clck();
}

//mise en place des ecouteurs avec la fonction clkBut pour chaque boutton
function clck() {
    let btn1 = clkBut(boutton1);
    let btn2 = clkBut(boutton2);
    let btn3 = clkBut(boutton3);
    let btn4 = clkBut(boutton4);
    let btn5 = clkBut(boutton5);
    let btn6 = clkBut(boutton6);
    let btn7 = clkBut(boutton7);
}

//rejouer
let play = document.getElementById("rejouer")
play.addEventListener("click", () => { location.reload() })

//lancement du jeu avec l'etat initial de la sphere
setTimeout(() => { sphere.good() }, 1700);

